#!/bin/sh
pandoc -t revealjs -s $1 -o $2 -V revealjs-url=https://revealjs.com -V theme=white -V transition=none
