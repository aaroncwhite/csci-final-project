---
title: Outlier detection
subtitle: using Luigi and Dask
author: Aaron White 
---

# Context

## Hip and knee replacements
can actually be interesting!


## Need 
* Identify hospitals outside of the norm
* Systematize adhoc analysis 
* Allow leadership easy access to bespoke analysis

## Work Goals

* Create a dashboard of 16 indicators
* Present data in accessible way for high level view and also "drill-down" capabilities

## Project Goals

* Create framework for other projects
* Force younger staff to learn with me :D

# Front end dashboard

## Specific indicator 
![](https://raw.githubusercontent.com/csci-e-29/final-project-aaroncwhite/master/presentation/img/dashboard2.png?token=8c2d97f30524d3b94a050e11fd56cb54821540f3)

## Comparing indicators
![](https://raw.githubusercontent.com/csci-e-29/final-project-aaroncwhite/master/presentation/img/dashboard3.png?token=8c2d97f30524d3b94a050e11fd56cb54821540f3)

## Comparing many indicators
![](https://raw.githubusercontent.com/csci-e-29/final-project-aaroncwhite/master/presentation/img/dashboard4.png?token=8c2d97f30524d3b94a050e11fd56cb54821540f3)

## Quick view
![](https://raw.githubusercontent.com/csci-e-29/final-project-aaroncwhite/master/presentation/img/dashboard1.png?token=8c2d97f30524d3b94a050e11fd56cb54821540f3)

# Data Calculations

## Flexible indicator definitions
Specific indicators use a declarative format to define which column to use
```python

class total_cost(Mean, Indicator):
    """Calculates the Average Total Cost 
    """
    column = 'epi_total'
```
Every indicator has a `.compute()` method that actually does the calculation.


## Flexible indicator structure
Setup becomes similar to defining a Luigi `task.output()` method

```python
class Mean:
    """Mixin for a default mean calculation on a dataframe

    self.column must be specified for this to work!
    """
    def compute(self, df):
        return df[self.column].mean()
```
All indicators share a base set of properties and use mixins to determine
calculation methods

## Flexible indicator structure
Or very specific calcuations can define `.compute()` directly
```python
class specific_indicator(Indicator):
    """A very specific calculation
    """
    def compute(self, df):
        # do lots of stuff here
        # ...
``` 

## Configuration
```json
{
"costs": { // This is an indicator group
    "indicators" : {
      "avg_total_cost" : {
        // ... other metadata used by front end 
        "calculation_method" : "total_cost",
      },
      "avg_post_epi_spend" : { 
        // ... other metadata used by front end 
        "calculation_method" : "post_epi_spending",
      }
    }
  }
}
```

## Creation
```python
>>> indicator_calculator = Calculator(config, 
                                      indicator_module)
>>> indicator_calculator(claims_df)
indicator                   value
costs.avg_total_cost        23521.024
costs.avg_post_epi_spend    2134.123
```


## Grouped calculations
`Calculator` works with pandas or dask dataframes using standard
`df.groupby().apply()` syntax.
```python
>>> disaggregations = ['provider', 'timeframe', 'drg_fracture']
>>> claims_df.groupby(disaggregations).apply(indicator_calculator)
provider timeframe drg_fracture indicator                value
1        baseline  41F          costs.avg_total_cost     23521.024
...      ...       ...          ...                      ...
20       perf      41NF         costs.avg_post_epi_spend 2134.123
...
```


## Luigi for high level control

![](img/dataflow.png)

## Dask graph running
![Reducing parquet dataset with three different timeframes](img/Dask2.PNG)

## Flask API
* Serves data to front end Vue dashboard
* Uses `flask-restplus` for swagger api definitions
* Marks outliers on request and serves in format ready for presentation

## Overkill?
* Allows indicators to be defined in a similar way every time
* Can also handle more complex calculations
* Lazy analysts can define test data immediately

## Luigi
![Success!](img/luigi2.PNG)

# The real point

## This wasn't just for fun
* Luigi + Dask have drastically reduced calculation times for processing large files
* Pieces are in place to schedule data processing automatically
* We can bolt on new indicators quickly

## Next steps
* Build out end to end tests
* Serve Vue front end files from backend server
* `salted` indicators to keep track of config/calculation changes

## If there's time and $$
* Using a real database instead of output files
* Allow multiple sources of indicator methods
* `Calculator` is built for one single dataframe with all data, but that isn't very realistic
* `df.aggregate()` might be more efficient than `df.apply()` for grouped calculations


## Modular potential
Other projects are doing similar work

# Additional Info

## Outlier Identification
* Calculated indicators per hospital for every disaggregation 
* Use Mahalanobis distance to identify values >=95% quantile
* Works with single or many indicators!

## Front end dashboard
* Shared configuration to define display layout
* Allows us to 'register' new calculations in one place
* We can version the configuration to keep track of changes over time


## Mixins

Implemented several mixins to add convenience methods to Luigi tasks

```python
class JsonImportMixin:
    """Mixin to add an import data function onto any task
    and return a parsed dict from a json file. 
    """

    def import_file(self, **kwargs):
        """Utility method to read the json file.  kwargs are passed to 
        json.load()
        """
        with self.output().open() as f:
            data = json.load(f, **kwargs)

        return data
```

## Dask Targets

Lightly modified local targets for use with Dask

```python
class DaskParquetTarget(LocalTarget):

    def exists(self):
        """Return true if _common_metadata found in self.path
        """
        if os.path.exists(self.path):
            return '_common_metadata' in os.listdir(self.path)
        else:
            return False
```

## Descriptors

Leveraged `Requires` and `Requirement` descriptors we developed in class

```python
class CalculateIndicators(JsonImportMixin, Task):
    """Calculates indicators of interest based on 
    a specified config file and pre-processed 
    episode and pac data.

    Requires:
        IndicatorConfig {ExternalTask} -- The configuration file
        CombineEpis {Task} -- The episode processing task
    """
    # ... Parameters
    requires = Requires()
    config = Requirement(IndicatorConfig) # Using default and expecting config in root dir
    episodes = Requirement(CombineEpis, output_name = 'episodes')
```



## Flexible structure
Instantiation returns a callable function that can be used on a dataframe.
```python
>>> epi_total_calculator = total_cost(name='my_output_label')
>>> epi_total_calculator(claims_df)
22453.345
```
`name` is not used yet, but will be in a second! -->


## Hypothesis baked in
Special descriptor method allows implementer to define expected data types for tests
```python
class total_cost(Mean, Indicator):
    """Calculates the Average Total Cost 
    """
    column = Column('epi_total', 
                    strategy='floats', 
                    strategy_kwargs=dict(max_value = 60000, 
                                         min_value = 1000))
```
During testing, a `data_column` is created using any `strategy` available from the hypothesis library.  




# Questions?